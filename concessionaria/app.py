from flask import Flask, request

app = Flask(__name__)

montadoras = [
    {"nome": "Fiat", "carros":
        [
            {"modelo": "Mobi", "preco": 60000, "ano_modelo":2024,"ano_fab":2023}
            {"modelo": "Argo", "preco": 79000, "ano_modelo":2024,"ano_fab":2023}
        ]
    },
        {"nome": "FORD", "carros":
        [
            {"modelo": "ka", "preco": 60000, "ano_modelo":2024,"ano_fab":2023}
            {"modelo": "ECOSPORT", "preco": 79000, "ano_modelo":2024,"ano_fab":2023}
        ]
    }

    ]

@app.get("/montadoras")
def get_montadoras():
    return {"montadoras": viagens}

@app.get("/montadoras/<string:nome>")
def get_montadora_by_nome(nome):
    for montadora in montadoras:
        if montadora["nome"] == nome:
            return montadora
    return {"message": "montadora not found"}, 404

@app.get("/montadoras/<string:montadora>/carros/")
def get_carros_in_montadora(montadora):
    for montadora in montadoras:
        if montadora["nome"] == montadora:
            return {"carros": montadora["carros"]}
    return {"message": "montadora not found"}, 404

@app.post("/montadoras")
def create_montadora():
    request_data = request.get_json() #pega o conteudo do body
    new_montadora = {"nome": request_data["nome"], "carros": []}
    montadoras.append(new_montadora) #insere o payload na montadora
    return new_montadora, 201

@app.post("/montadoras/<string:montadora>/carro")
def create_carro(montadora):
    request_data = request.get_json()
    for montadora in montadoras:
        if montadora["nome"] == montadora:
            new_carro = {
                "modelo": request_data["modelo"],
                 "preco": request_data["preco"],
                  "ano_modelo":request_data["ano_modelo"],
                  "ano_fab":request_data["ano_fab"]
                  }
            montadora["nome"].append(new_carro)
            return new_carro, 201
    return {"message": "Montadora nao encontrada"}, 404


if __name__ == '__main__':
    import uvicorn

    uvicorn.run("main:app", host="0.0.0.0", port=8000, debug=True, reload=True)